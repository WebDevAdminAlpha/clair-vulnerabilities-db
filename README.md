This project is used to mirror the Docker image 
[`arminc/clair-db`](https://hub.docker.com/r/arminc/clair-db) used in
[Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html)

